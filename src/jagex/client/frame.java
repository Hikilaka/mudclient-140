package jagex.client;

import java.awt.*;

@SuppressWarnings("serial")
public class frame extends Frame {

	int fw, fh;
	int trans, off;
	game fg;
	Graphics gfx;
	
	public frame(game g, int w, int h, String title, boolean r, boolean b) {
		off = 28;
		this.fw = w;
		this.fh = h;
		this.fg = g;
		if(b) {
			off = 48;
		} else {
			off = 28;
		}
		setTitle(title);
		setResizable(r);
		setVisible(true);
		toFront();
		resize(w, h);
		gfx = getGraphics();
	}

	@Override
	public Graphics getGraphics() {
		Graphics g = super.getGraphics();
		if(trans == 0)
			g.translate(0, 24);
		else
			g.translate(-5, 0);
		return g;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void resize(int w, int h) {
		super.resize(w, h + off);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean handleEvent(Event event) {
		if(event.id == 401) {
			fg.keyDown(event, event.key);
		} else if (event.id == 402) {
			fg.keyUp(event, event.key);
		} else if(event.id == 501) {
			fg.mouseDown(event, event.x, event.y - 24);
		} else if(event.id == 506) {
			fg.mouseDrag(event, event.x, event.y - 24);
		} else if(event.id == 502) {
			fg.mouseUp(event, event.x, event.y - 24);
		} else if(event.id == 503) {
			fg.mouseMove(event, event.x, event.y - 24);
		} else if(event.id == 201) {
			fg.destroy();
		} else if(event.id == 1001) {
			fg.action(event, event.target);
		} else if(event.id == 403) {
			fg.keyDown(event, event.key);
		} else if(event.id == 404) {
			fg.keyUp(event, event.key);
		}
		return true;
	}

	@Override
	public final void paint(Graphics g) {
		fg.paint(g);
	}

}