package jagex.client;

import jagex.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class connection extends socket implements Runnable {

	private final InputStream in;
	
	private final OutputStream out;
	
	private final Socket sock;
	
	private boolean closing, closed;
	
	private byte data[];
	
	private int offset, pos;
	
	private Thread thread;
		
	public connection(game g, String host, int port) throws IOException {
		closing = false;
		closed = true;
		if (g.gappmode()) {
			sock = new Socket(InetAddress.getByName(g.getCodeBase().getHost()), port);
		} else {
			sock = new Socket(InetAddress.getByName(host), port);
		}
		sock.setSoTimeout(30000);
		sock.setTcpNoDelay(true);
		in = sock.getInputStream();
		out = sock.getOutputStream();
		//g.start(this);
	}

	public void close() {
		super.close();
		closing = true;
		try {
			if (in != null) in.close();
			if (out != null) out.close();
			if (sock != null) sock.close();
		} catch(IOException e) {
			System.out.println("Error closing stream");
		}
		if (thread != null) {
			closed = true;
			synchronized (this) {
				notify();
			}
			thread = null;
		}
		data = null;
	}

	public int read() throws IOException {
		if (closing) {
			return 0;
		} else {
			return in.read();
		}
	}

	public int available() throws IOException {
		if (closing) {
			return 0;
		} else {
			return in.available();
		}
	}

	public void read(int len, int off, byte buf[]) throws IOException {
		if(closing) return;
		
		int i = 0, dat;
		for (; i < len; i += dat) {
			if ((dat = in.read(buf, i + off, len - i)) <= 0) {
				throw new IOException("EOF");
			}
		}
	}

	public void writebuf(byte[] buf, int off, int len) throws IOException {
		if (closing) return;
		if (data == null) data = new byte[5000];
		synchronized (this) {
			for (int i = 0; i < len; i++) {
				data[pos] = buf[i + off];
				pos = (pos + 1) % 5000;
				if (pos == (offset + 4900) % 5000) { 
					throw new IOException("buffer overflow");
				}
			}

			if (thread == null) {
				closed = false;
				thread = new Thread(this);
				thread.setDaemon(true);
				thread.setPriority(4);
				thread.start();
			}
			notify();
		}
	}

	public void run() {
		while (thread != null && !closed) {
			int len;
			int off;
			synchronized (this) {
				if (pos == offset) {
					try {
						wait();
					} catch (InterruptedException e) {
					}
				}
				if (thread == null || closed) return;
				off = offset;
				if (pos >= offset) {
					len = pos - offset;
				} else {
					len = 5000 - offset;
				}
			}
			if (len > 0) {
				try {
					out.write(data, off, len);
				} catch(IOException e) {
					super.error = true;
					super.errortext = "Twriter:" + e;
				}
				offset = (offset + len) % 5000;
				try {
					if(pos == offset)
						out.flush();
				} catch(IOException e) {
					super.error = true;
					super.errortext = "Twriter:" + e;
				}
			}
		}
	}

}