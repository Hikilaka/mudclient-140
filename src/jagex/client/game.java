package jagex.client;

import jagex.bzip;
import jagex.util;
import java.applet.Applet;
import java.awt.*;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.*;

@SuppressWarnings("serial")
public class game extends Applet implements Runnable {

	public game() {
		width = 512;
		height = 384;
		refreshrate = 20;
		pp = 1000;
		profiles = new long[10];
		state = 1;
		updating = false;
		loadingText = "Loading";
		roman = new Font("TimesRoman", 0, 15);
		bhelvet = new Font("Helvetica", 1, 13);
		helvet = new Font("Helvetica", 0, 12);
		openkey = false;
		clodekey = false;
		leftkey = false;
		rightkey = false;
		upkey = false;
		downkey = false;
		spacekey = false;
		nmkey = false;
		qq = 1;
		f1key = false;
		inputtext = "";
		enteredtext = "";
		inputmessage = "";
		enteredmessage = "";
	}

	public void load() { }

	public synchronized void tick() { }

	public synchronized void render() { }

	public void onpaint() { }
	
	public void onkill() { }

	public void onkey(int code) { }

	public void onclick(int click, int x, int y) { }

	public final void mkwin(int w, int h, String title, boolean resize) {
		appletmode = false;
		System.out.println("Started application");
		width = w;
		height = h;
		frame = new frame(this, w, h, title, resize, false);
		state = 1;
		thread = new Thread(this);
		thread.start();
		thread.setPriority(1);
	}

	public final boolean gappmode() {
		return appletmode;
	}

	@Override
	public final Graphics getGraphics() {
		if(frame == null) {
			return super.getGraphics();
		} else {
			return frame.getGraphics();
		}
	}

	@Override
	public final int getWidth() {
		return width;
	}

	@Override
	public final int getHeight() {
		return height;
	}

	@Override
	public final Image createImage(int w, int h) {
		if (frame == null) {
			return super.createImage(w, h);
		} else {
			return frame.createImage(w, h);
		}
	}

	public Frame gframe() {
		return frame;
	}

	public final void setRefreshRate(int rate) {
		refreshrate = 1000 / rate;
	}

	public final void rj(int j) {
		pp = j;
	}

	public final void clearprof() {
		for (int i = 0; i < 10; i++) {
			profiles[i] = 0L;
		}
	}

	@Override
	public synchronized boolean keyDown(Event event, int code) {
		onkey(code);
		vq = code;
		wq = code;
		lasttimeout = 0;
		if (code == 1006)
			leftkey = true;
		if (code == 1007)
			rightkey = true;
		if (code == 1004)
			upkey = true;
		if (code == 1005)
			downkey = true;
		if ((char)code == ' ')
			spacekey = true;
		if ((char)code == 'n' || (char)code == 'm')
			nmkey = true;
		if ((char)code == 'N' || (char)code == 'M')
			nmkey = true;
		if ((char)code == '{')
			openkey = true;
		if ((char)code == '}')
			clodekey = true;
		if ((char)code == '\u03F0')
			f1key = !f1key;

		boolean valid = false;
		for (int i = 0; i < charset.length(); i++) {
			if (code != charset.charAt(i)) {
				continue;
			}
			valid = true;
			break;
		}

		if (valid && inputtext.length() < 20)
			inputtext += (char)code;
		if (valid && inputmessage.length() < 80)
			inputmessage += (char)code;
		if (code == 8 && inputtext.length() > 0)
			inputtext = inputtext.substring(0, inputtext.length() - 1);
		if (code == 8 && inputmessage.length() > 0)
			inputmessage = inputmessage.substring(0, inputmessage.length() - 1);

		if (code == 10 || code == 13) {
			enteredtext = inputtext;
			enteredmessage = inputmessage;
		}
		return true;
	}

	@Override
	public synchronized boolean keyUp(Event event, int code) {
		vq = 0;
		if (code == 1006)
			leftkey = false;
		if (code == 1007)
			rightkey = false;
		if (code == 1004)
			upkey = false;
		if (code == 1005)
			downkey = false;
		if ((char)code == ' ')
			spacekey = false;
		if ((char)code == 'n' || (char)code == 'm')
			nmkey = false;
		if ((char)code == 'N' || (char)code == 'M')
			nmkey = false;
		if ((char)code == '{')
			openkey = false;
		if ((char)code == '}')
			clodekey = false;
		return true;
	}

	@Override
	public synchronized boolean mouseMove(Event event, int x, int y) {
		mousex = x;
		mousey = y + mouseoffset;
		mouseclick = 0;
		lasttimeout = 0;
		return true;
	}

	@Override
	public synchronized boolean mouseUp(Event event, int x, int y) {
		mousex = x;
		mousey = y + mouseoffset;
		mouseclick = 0;
		return true;
	}

	@Override
	public synchronized boolean mouseDown(Event event, int x, int y) {
		mousex = x;
		mousey = y + mouseoffset;
		if (event.metaDown())
			mouseclick = 2;
		else
			mouseclick = 1;
		lastclick = mouseclick;
		lasttimeout = 0;
		onclick(mouseclick, x, y);
		return true;
	}

	@Override
	public synchronized boolean mouseDrag(Event event, int x, int y) {
		mousex = x;
		mousey = y + mouseoffset;
		if (event.metaDown())
			mouseclick = 2;
		else
			mouseclick = 1;
		return true;
	}

	@Override
	public final void init() {
		appletmode = true;
		System.out.println("Started applet");
		width = getSize().width;
		height = getSize().height;
		state = 1;
		util.codebase = getCodeBase();
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public final void start() {
		if (killstat >= 0)
			killstat = 0;
	}

	@Override
	public final void stop() {
		if (killstat >= 0)
			killstat = 4000 / refreshrate;
	}

	@SuppressWarnings("deprecation")
	@Override
	public final void destroy() {
		killstat = -1;
		try {
			Thread.sleep(5000L);
		} catch(Exception e) { }
		if(killstat == -1) {
			System.out.println("5 seconds expired, forcing kill");
			kill();
			if(thread != null) {
				thread.stop();
				thread = null;
			}
		}
	}

	public final void kill() {
		killstat = -2;
		System.out.println("Closing program");
		onkill();
		try {
			Thread.sleep(1000L);
		} catch(Exception e) { }
		if (frame != null) {
			frame.dispose();
		}
		if (!appletmode) {
			System.exit(0);
		}
	}

	public final void run() {
		if (state == 1) {
			state = 2;
			loadinggfx = getGraphics();
			jaglib();
			showloadpercent(0, "Loading...");
			load();
			state = 0;
		}
		int j = 0;
		int i1 = 256;
		int j1 = 1;
		int k1 = 0;
		for (int i2 = 0; i2 < 10; i2++)
			profiles[i2] = System.currentTimeMillis();

		while (killstat >= 0) {
			if (killstat > 0) {
				killstat--;
				if (killstat == 0) {
					kill();
					thread = null;
					return;
				}
			}
			int j2 = i1;
			int k2 = j1;
			i1 = 300;
			j1 = 1;
			long l1 = System.currentTimeMillis();
			if (profiles[j] == 0L) {
				i1 = j2;
				j1 = k2;
			} else if (l1 > profiles[j])
				i1 = (int)((long)(2560 * refreshrate) / (l1 - profiles[j]));
			if (i1 < 25)
				i1 = 25;
			if (i1 > 256) {
				i1 = 256;
				j1 = (int)((long)refreshrate - (l1 - profiles[j]) / 10L);
				if (j1 < qq)
					j1 = qq;
			}
			try {
				Thread.sleep(j1);
			} catch(InterruptedException e) { }
			profiles[j] = l1;
			j = (j + 1) % 10;
			if (j1 > 1) {
				for(int l2 = 0; l2 < 10; l2++)
					if(profiles[l2] != 0L)
						profiles[l2] += j1;
			}
			int i3 = 0;
			while (k1 < 256) {
				tick();
				k1 += i1;
				if (++i3 > pp) {
					k1 = 0;
					up += 6;
					if (up > 25) {
						up = 0;
						f1key = true;
					}
					break;
				}
			}
			up--;
			k1 &= 0xff;
			render();
		}
		if (killstat == -1)
			kill();
		thread = null;
	}

	@Override
	public final void update(Graphics g) {
		paint(g);
	}

	@Override
	public final void paint(Graphics g) {
		if (state == 2 && jagLogo != null) {
			showloadpercent(loadingPercent, loadingText);
			return;
		}
		if (state == 0)
			onpaint();
	}

	public void jaglib() {
		try {
			byte arc[] = util.garc("release/jagex.jag");
			byte logo[] = util.getf("logo.tga", 0, arc);
			jagLogo = mklogo(logo);
			graphics.loadfont(util.getf("h11p.jf", 0, arc));
			graphics.loadfont(util.getf("h12b.jf", 0, arc));
			graphics.loadfont(util.getf("h12p.jf", 0, arc));
			graphics.loadfont(util.getf("h13b.jf", 0, arc));
			graphics.loadfont(util.getf("h14b.jf", 0, arc));
			graphics.loadfont(util.getf("h16b.jf", 0, arc));
			graphics.loadfont(util.getf("h20b.jf", 0, arc));
			graphics.loadfont(util.getf("h24b.jf", 0, arc));
		} catch(IOException e) {
			System.out.println("Error loading jagex.dat");
		}
	}

	public void showloadpercent(int percent, String str) {
		int x = (width - 281) / 2;
		int y = (height - 148) / 2;
		loadinggfx.setColor(Color.black);
		loadinggfx.fillRect(0, 0, width, height);
		if (!updating)
			loadinggfx.drawImage(jagLogo, x, y, this);
		x += 2;
		y += 90;
		loadingPercent = percent;
		loadingText = str;
		loadinggfx.setColor(new Color(132, 132, 132));
		if (updating)
			loadinggfx.setColor(new Color(220, 0, 0));

		loadinggfx.drawRect(x - 2, y - 2, 280, 23);
		loadinggfx.fillRect(x, y, (277 * percent) / 100, 20);
		loadinggfx.setColor(new Color(198, 198, 198));
		if (updating)
			loadinggfx.setColor(new Color(255, 255, 255));
		drawstr(loadinggfx, str, roman, x + 138, y + 10);

		if (!updating) {
			drawstr(loadinggfx, "Created by JAGeX - visit www.jagex.com", bhelvet, x + 138, y + 30);
			drawstr(loadinggfx, "\2512001-2002 Andrew Gower and Jagex Ltd", bhelvet, x + 138, y + 44);
		} else {
			loadinggfx.setColor(new Color(132, 132, 152));
			drawstr(loadinggfx, "\2512001-2002 Andrew Gower and Jagex Ltd", helvet, x + 138, height - 20);
		}

		if (yp != null) {
			loadinggfx.setColor(Color.white);
			drawstr(loadinggfx, yp, bhelvet, x + 138, y - 120);
		}
	}

	public void showpercent(int percent, String str) {
		int x = (width - 281) / 2;
		int y = (height - 148) / 2;
		x += 2;
		y += 90;
		loadingPercent = percent;
		loadingText = str;
		int j1 = (277 * percent) / 100;
		loadinggfx.setColor(new Color(132, 132, 132));
		if (updating) {
			loadinggfx.setColor(new Color(220, 0, 0));
		}
		loadinggfx.fillRect(x, y, j1, 20);
		loadinggfx.setColor(Color.black);
		loadinggfx.fillRect(x + j1, y, 277 - j1, 20);
		loadinggfx.setColor(new Color(198, 198, 198));
		if (updating) {
			loadinggfx.setColor(new Color(255, 255, 255));
		}
		drawstr(loadinggfx, str, roman, x + 138, y + 10);
	}

	public void drawstr(Graphics graphics, String str, Font font, int x, int y) {
		Component component;
		if (frame == null) {
			component = this;
		} else {
			component = frame;
		}
		FontMetrics metrics = component.getFontMetrics(font);
		graphics.setFont(font);
		graphics.drawString(str, x - metrics.stringWidth(str) / 2, y + metrics.getHeight() / 4);
	}

	public Image mklogo(byte[] data) {
		int width = data[13] * 256 + data[12];
		int height = data[15] * 256 + data[14];
		byte r[] = new byte[256];
		byte g[] = new byte[256];
		byte b[] = new byte[256];

		for (int i = 0; i < 256; i++) {
			r[i] = data[20 + i * 3];
			g[i] = data[19 + i * 3];
			b[i] = data[18 + i * 3];
		}

		IndexColorModel colormodel = new IndexColorModel(8, 256, r, g, b);
		byte pixels[] = new byte[width * height];
		int indx = 0;
		for (int y = height - 1; y >= 0; y--) {
			for (int x = 0; x < width; x++) {
				pixels[indx++] = data[786 + x + y * width];
			}
		}

		MemoryImageSource memoryimagesource = new MemoryImageSource(width, height, colormodel, pixels, 0, width);
		Image image = createImage(memoryimagesource);
		return image;
	}

	public byte[] loadfile(String file, String title, int percent) throws IOException {
		int state = 0;
		int decomplen = 0;
		int complen = 0;
		byte[] d = null;
		while (state < 2)
			try {
				showpercent(percent, "Loading " + title + " - 0%");
				if (state == 1)
					file = file.toUpperCase();
				InputStream istream = util.openstream(file);
				DataInputStream stream = new DataInputStream(istream);
				byte headers[] = new byte[6];
				stream.readFully(headers, 0, 6);
				decomplen = ((headers[0] & 0xff) << 16) + ((headers[1] & 0xff) << 8) + (headers[2] & 0xff);
				complen = ((headers[3] & 0xff) << 16) + ((headers[4] & 0xff) << 8) + (headers[5] & 0xff);
				showpercent(percent, "Loading " + title + " - 5%");
				int offset = 0;
				d = new byte[complen];
				while (offset < complen) {
					int block = complen - offset;
					if (block > 1000)
						block = 1000;
					stream.readFully(d, offset, block);
					offset += block;
					showpercent(percent, "Loading " + title + " - " + (5 + (offset * 95) / complen) + "%");
				}
				state = 2;
				stream.close();
			} catch(IOException e) {
				state++;
			}
			showpercent(percent, "Unpacking " + title);
			if (complen != decomplen) {
				byte abyte1[] = new byte[decomplen];
				bzip.decompress(abyte1, decomplen, d, complen, 0);
				return abyte1;
			} else {
				return d;
			}
	}

	private int width;
	private int height;
	private Thread thread;
	private int refreshrate;
	private int pp;
	private long profiles[];
	static frame frame = null;
	private boolean appletmode;
	private int killstat;
	private int up;
	public int mouseoffset;
	public int lasttimeout;
	public int state;
	public String yp;
	private boolean updating;
	private int loadingPercent;
	private String loadingText;
	private Font roman;
	private Font bhelvet;
	private Font helvet;
	private Image jagLogo;
	private Graphics loadinggfx;
	private static String charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\243$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
	public boolean openkey;
	public boolean clodekey;
	public boolean leftkey;
	public boolean rightkey;
	public boolean upkey;
	public boolean downkey;
	public boolean spacekey;
	public boolean nmkey;
	public int qq;
	public int mousex;
	public int mousey;
	public int mouseclick;
	public int lastclick;
	public int vq;
	public int wq;
	public boolean f1key;
	public String inputtext;
	public String enteredtext;
	public String inputmessage;
	public String enteredmessage;

}