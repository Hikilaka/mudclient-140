package jagex.client;

import jagex.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.math.BigInteger;

@SuppressWarnings("serial")
public class networkedgame extends game {

	public static String[] responses;
	public static int clientver = 1;
	public static int packetread;
	public String host;
	public int port;
	String username,  password;
	public connection stream;
	byte packetdata[];
	int reconattempts;
	long lastping;
	public int friendcnt, ignorecnt;
	public long[] friends, ignores;
	public int friendw[];
	public int blockchat;
	public int blockpriv;
	public int blocktrade;
	public int blockduel;
	public BigInteger exponent, modulus;
	public int sessionid,  socktimeout;

	public networkedgame() {
		host = "127.0.0.1";
		port = 43594;
		username = "";
		password = "";
		packetdata = new byte[5000];
		friends = new long[100];
		friendw = new int[100];
		ignores = new long[50];
	}

	public void setloginstatus(String s1, String s2) { }

	public void onreconnect() { }

	public void onlogin() { }

	public void resetstates() { }

	public void logoutrefused() { }

	public void relog() { }

	public void onpacket(int id, int len, byte[] buf) { }

	public void showmsg(String msg) { }

	public boolean w() {
		return true;
	}

	public void setrsakeys(BigInteger exponent, BigInteger modulus) {
		this.exponent = exponent;
		this.modulus = modulus;
	}

	public int getseed() {
		try {
			String ranseed = getParameter("ranseed");
			ranseed = ranseed.substring(0, 10);
			int seed = Integer.parseInt(ranseed);

			if (seed == 987654321) {
				byte buffer[] = new byte[4];
				util.readfile("uid.dat", buffer, 4);
				seed = jagex.util.g4(buffer, 0);
			}
			return seed;
		} catch(Exception e) {
			return 0;
		}
	}

	public void login(String user, String pass, boolean reconnect) {
		if(socktimeout > 0) {
			setloginstatus(responses[6], responses[7]);
			try {
				Thread.sleep(2000L);
			} catch (Exception e) { }
			setloginstatus(responses[8], responses[9]);
			return;
		}
		try {
			username = user;
			user = jagex.util.fmtstr(user, 20);
			password = pass;
			pass = jagex.util.fmtstr(pass, 20);
			
			if (user.trim().length() == 0) {
				setloginstatus(responses[0], responses[1]);
				return;
			}
			
			if (reconnect) {
				showdialog(responses[2], responses[3]);
			} else {
				setloginstatus(responses[6], responses[7]);
			}

			stream = new connection(this, host, port);
			stream.maxcnt = packetread;
			
			int sessionId = stream.g4();
			sessionid = sessionId;
			System.out.println("Session id: " + sessionId);
			int referid = 0;
			try {
				if (gappmode()) {
					String referrer = getParameter("referid");
					referid = Integer.parseInt(referrer);
				}
			} catch (Exception e) { }

			if (reconnect) {
				stream.create(19);
			} else {
				stream.create(0);
			}

			stream.p2(clientver);
			stream.p2(referid);
			stream.p8(jagex.util.encodeb37(user));
			stream.prsastr(pass, sessionId, exponent, modulus);
			stream.p4(getseed());
			stream.flush();
			stream.read();
			int reponse = stream.read();
			System.out.println("Login response: " + reponse);
			if (reponse == 0) {
				reconattempts = 0;
				onlogin();
				return;
			}
			if (reponse == 1) {
				reconattempts = 0;
				onreconnect();
				return;
			}
			if (reconnect) {
				user = "";
				pass = "";
				resetstates();
				return;
			}
			if (reponse == 3) {
				setloginstatus(responses[10], responses[11]);
				return;
			}
			if (reponse == 4) {
				setloginstatus(responses[4], responses[5]);
				return;
			}
			if (reponse == 5) {
				setloginstatus(responses[16], responses[17]);
				return;
			}
			if (reponse == 6) {
				setloginstatus(responses[18], responses[19]);
				return;
			}
			if (reponse == 7) {
				setloginstatus(responses[20], responses[21]);
				return;
			}
			if (reponse == 11) {
				setloginstatus(responses[22], responses[23]);
				return;
			}
			if (reponse == 12) {
				setloginstatus(responses[24], responses[25]);
				return;
			}
			if (reponse == 13) {
				setloginstatus(responses[14], responses[15]);
				return;
			}
			if (reponse == 14) {
				setloginstatus(responses[8], responses[9]);
				socktimeout = 1500;
				return;
			}
			if (reponse == 15) {
				setloginstatus(responses[26], responses[27]);
				return;
			}
			if (reponse == 16) {
				setloginstatus(responses[28], responses[29]);
				return;
			} else {
				setloginstatus(responses[12], responses[13]);
				return;
			}
		} catch (Exception e) {
			System.out.println(String.valueOf(e));
		}
		if (reconattempts > 0) {
			try {
				Thread.sleep(5000L);
			} catch(Exception w) { }
			reconattempts--;
			login(username, password, reconnect);
		}
		if (reconnect) {
			username = "";
			password = "";
			resetstates();
		} else {
			setloginstatus(responses[12], responses[13]);
		}
	}

	public void sendlogout() {
		if (stream != null) {
			try {
				stream.create(1);
				stream.flush();
			} catch(IOException _ex) { }
		}
		username = "";
		password = "";
		resetstates();
	}

	public void connectionlost() {
		System.out.println("Lost connection");
		reconattempts = 10;
		login(username, password, true);
	}

	public void showdialog(String str1, String str2) {
		Graphics graphics = getGraphics();
		Font font = new Font("Helvetica", 1, 15);
		int width = getWidth();
		int height = getHeight();
		graphics.setColor(Color.black);
		graphics.fillRect(width / 2 - 140, height / 2 - 25, 280, 50);
		graphics.setColor(Color.white);
		graphics.drawRect(width / 2 - 140, height / 2 - 25, 280, 50);
		drawstr(graphics, str1, font, width / 2, height / 2 - 10);
		drawstr(graphics, str2, font, width / 2, height / 2 + 10);
	}

	public void newplayer(String user, String pass) {
		if (socktimeout > 0) {
			setloginstatus(responses[6], responses[7]);
			try {
				Thread.sleep(2000L);
			} catch(Exception e) { }
			setloginstatus(responses[8], responses[9]);
			return;
		}
		try {
			user = util.fmtstr(user, 20);
			pass = util.fmtstr(pass, 20);
			setloginstatus(responses[6], responses[7]);

			stream = new connection(this, host, port);
			
			int sessionId = stream.g4();
			sessionid = sessionId;
			System.out.println("Session id: " + sessionId);
			int referid = 0;
			try {
				if (gappmode()) {
					String referstr = getParameter("referid");
					referid = Integer.parseInt(referstr);
				}
			} catch(Exception e) { }
			stream.create(2);
			stream.p2(clientver);
			stream.p8(util.encodeb37(user));
			stream.p2(referid);
			stream.prsastr(pass, sessionId, exponent, modulus);
			stream.p4(getseed());
			stream.flush();
			stream.read();
			int response = stream.read();
			stream.close();
			System.out.println("Newplayer response: " + response);
			if (response == 2) {
				relog();
				return;
			}
			if (response == 3) {
				setloginstatus(responses[14], responses[15]);
				return;
			}
			if (response == 4) {
				setloginstatus(responses[4], responses[5]);
				return;
			}
			if (response == 5) {
				setloginstatus(responses[16], responses[17]);
				return;
			}
			if (response == 6) {
				setloginstatus(responses[18], responses[19]);
				return;
			}
			if (response == 7) {
				setloginstatus(responses[20], responses[21]);
				return;
			}
			if (response == 11) {
				setloginstatus(responses[22], responses[23]);
				return;
			}
			if (response == 12) {
				setloginstatus(responses[24], responses[25]);
				return;
			}
			if (response == 13) {
				setloginstatus(responses[14], responses[15]);
				return;
			}
			if (response == 14) {
				setloginstatus(responses[8], responses[9]);
				socktimeout = 1500;
				return;
			}
			if (response == 15) {
				setloginstatus(responses[26], responses[27]);
				return;
			}
			if (response == 16) {
				setloginstatus(responses[28], responses[29]);
				return;
			} else {
				setloginstatus(responses[12], responses[13]);
				return;
			}
		} catch(Exception e) {
			System.out.println(String.valueOf(e));
		}
		setloginstatus(responses[12], responses[13]);
	}

	public void pingnread() {
		long now = System.currentTimeMillis();
		if (stream.any()) {
			lastping = now;
		}
		if (now - lastping > 5000L) {
			lastping = now;
			stream.create(5);
			stream.fmtdata();
		}
		try {
			stream.wdata(20);
		} catch(IOException e) {
			connectionlost();
			return;
		}
		if (!w())
			return;
		int len = stream.readbuf(packetdata);
		if (len > 0) {
			handlepacket(packetdata[0] & 0xff, len);
		}
	}

	public void handlepacket(int id, int len) {
		if(id == 8) { //send message
			String str = new String(packetdata, 1, len - 1);
			showmsg(str);
		} else if(id == 9) { //send logout
			sendlogout();
		} else if(id == 10) { //cant logout
			logoutrefused();
		} else if(id == 23) { //init friend list
			friendcnt = jagex.util.g1(packetdata[1]);

			for(int i = 0; i < friendcnt; i++) {
				friends[i] = jagex.util.g8(packetdata, 2 + i * 9);
				friendw[i] = jagex.util.g1(packetdata[10 + i * 9]);
			}
			sortfriends();
		} else if(id == 24) { //update friend list
			long hash = jagex.util.g8(packetdata, 1);
			int world = packetdata[9] & 0xff;

			for(int i = 0; i < friendcnt; i++) {
				if(friends[i] == hash) {
					if(friendw[i] == 0 && world != 0)
						showmsg("@pri@" + jagex.util.decodeb37(hash) + " has logged in");
					if(friendw[i] != 0 && world == 0)
						showmsg("@pri@" + jagex.util.decodeb37(hash) + " has logged out");
					friendw[i] = world;
					len = 0;
					sortfriends();
					return;
				}
			}

			friends[friendcnt] = hash;
			friendw[friendcnt] = world;
			friendcnt++;
			showmsg("@pri@" + jagex.util.decodeb37(hash) + " has been added to your friends list");
			sortfriends();
		} else if(id == 26) { //init ignore list
			ignorecnt = jagex.util.g1(packetdata[1]);
			for(int i = 0; i < ignorecnt; i++)
				ignores[i] = jagex.util.g8(packetdata, 2 + i * 8);
		} else if(id == 27) { //settings update
			blockchat = packetdata[1];
			blockpriv = packetdata[2];
			blocktrade = packetdata[3];
			blockduel = packetdata[4];
		} else if(id == 28) { //recieved pm
			long hash = jagex.util.g8(packetdata, 1);
			String msg = jagex.util.nn(packetdata, 9, len - 9, true);
			showmsg("@pri@" + jagex.util.decodeb37(hash) + ": tells you " + msg);
		} else {
			onpacket(id, len, packetdata);
		}
	}

	public void sortfriends() {
		boolean active = true;
		while (active) {
			active = false;
			for (int i = 0; i < friendcnt - 1; i++) {
				if (friendw[i] < friendw[i + 1]) {
					int world = friendw[i];
					friendw[i] = friendw[i + 1];
					friendw[i + 1] = world;
					long hash = friends[i];
					friends[i] = friends[i + 1];
					friends[i + 1] = hash;
					active = true;
				}
			}
		}
	}

	public void sendrecov(String user, String pass) {
		user = jagex.util.fmtstr(user, 20);
		pass = jagex.util.fmtstr(pass, 20);
		stream.create(25);
		stream.prsastr(user + pass, sessionid, exponent, modulus);
		stream.fmtdata();
	}

	public void updatesettings(int i1, int j1, int k1, int l1) {
		stream.create(31);
		stream.p1(i1);
		stream.p1(j1);
		stream.p1(k1);
		stream.p1(l1);
		stream.fmtdata();
	}

	public void addignore(String username) {
		long hash = jagex.util.encodeb37(username);
		stream.create(29);
		stream.p8(hash);
		stream.fmtdata();

		for (int i = 0; i < ignorecnt; i++) {
			if (ignores[i] == hash) {
				return;
			}
		}

		if(ignorecnt >= 50) {
			return;
		} else {
			ignores[ignorecnt++] = hash;
			return;
		}
	}

	public void removeignore(long hash) {
		stream.create(30);
		stream.p8(hash);
		stream.fmtdata();
		for(int i = 0; i < ignorecnt; i++) {
			if(ignores[i] == hash) {
				ignorecnt--;
				for(int j = i; j < ignorecnt; j++) {
					ignores[j] = ignores[j + 1];
				}
				return;
			}
		}
	}

	public void addfriend(String username) {
		stream.create(26);
		stream.p8(jagex.util.encodeb37(username));
		stream.fmtdata();
	}

	public void removefriend(long hash) {
		stream.create(27);
		stream.p8(hash);
		stream.fmtdata();
		for (int i = 0; i < friendcnt; i++) {
			if (friends[i] != hash)
				continue;
			friendcnt--;
			for (int j = i; j < friendcnt; j++) {
				friends[j] = friends[j + 1];
				friendw[j] = friendw[j + 1];
			}
			break;
		}
		showmsg("@pri@" + jagex.util.decodeb37(hash) + " has been removed from your friends list");
	}

	public void sendpm(long hash, byte[] data, int len) {
		stream.create(28);
		stream.p8(hash);
		stream.pdata(data, 0, len);
		stream.fmtdata();
	}

	public void sendchat(byte[] data, int len) {
		stream.create(3);
		stream.pdata(data, 0, len);
		stream.fmtdata();
	}

	public void sendcmd(String cmd) {
		stream.create(7);
		stream.pjstr(cmd);
		stream.fmtdata();
	}

	static 
	{
		responses = new String[50];
		responses[0] = "You must enter both a username";
		responses[1] = "and a password - Please try again";
		responses[2] = "Connection lost! Please wait...";
		responses[3] = "Attempting to re-establish";
		responses[4] = "That username is already in use.";
		responses[5] = "Wait 60 seconds then retry";
		responses[6] = "Please wait...";
		responses[7] = "Connecting to server";
		responses[8] = "Sorry! The server is currently full.";
		responses[9] = "Please try again later";
		responses[10] = "Invalid username or password.";
		responses[11] = "Try again, or create a new account";
		responses[12] = "Sorry! Unable to connect to server.";
		responses[13] = "Check your internet settings";
		responses[14] = "Username already taken.";
		responses[15] = "Please choose another username";
		responses[16] = "The client has been updated.";
		responses[17] = "Please reload this page";
		responses[18] = "You may only use 1 character at once.";
		responses[19] = "Your ip-address is already in use";
		responses[20] = "Login attempts exceeded!";
		responses[21] = "Please try again in 5 minutes";
		responses[22] = "Account has been temporarily disabled";
		responses[23] = "for cheating or abuse";
		responses[24] = "Account has been permanently disabled";
		responses[25] = "for cheating or abuse";
		responses[26] = "You need a members account";
		responses[27] = "to login to this server";
		responses[28] = "Please login to a members server";
		responses[29] = "to access member-only features";
	}
}