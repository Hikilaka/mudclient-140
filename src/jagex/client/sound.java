package jagex.client;

import java.io.InputStream;
//import sun.audio.AudioPlayer;

/**
 * Plays audio using a depreciated library. It
 * has been removed in the latest JDK (1.7)
 * 
 * TODO: uhm, since audio player has been removed
 * we should probably update this to use
 * javax.sound.sampled?
 */
public final class sound extends InputStream {

	/**
	 * The data this {@link InputStream}
	 * will read from
	 */
	private byte[] data;
	
	/**
	 * The offset of this {@link InputStream}
	 */
	private int offset;
	
	/**
	 * The length of this {@link InputStream}
	 */
	private int length;
	
	public sound() {
		//AudioPlayer.player.start(this);
	}

	/**
	 * Stop playing a stream. The stream will stop playing,
	 * nothing happens if the stream wasn't playing in the first place.
	 */
	public void stop() {
		//AudioPlayer.player.stop(this);
	}

	/**
	 * Sets this {@link sound}'s <code>data</code>,
	 * <code>offset</code>, and <code>length</code>.
	 * 
	 * @param data The new data
	 * @param offset The new data offset
	 * @param length The new length
	 */
	public void set(byte[] data, int offset, int length) {
		this.data = data;
		this.offset = offset;
		this.length = offset + length;
	}

	/**
	 * Reads up to <code>len</code> bytes of data from the input stream
	 * into an array of bytes. An attempt is made to read
	 * as many as <code>len</code> bytes, but a smaller number may be read,
	 * possibly zero. The number of bytes actually read is
	 * returned as an integer. 
	 * 
	 * @param buffer the buffer into which the data is read.
	 * @param offset the start offset in array buffer at which the data is written.
	 * @param length the maximum number of bytes to read. 
	 * @return the total number of bytes read into the buffer,
	 * 		or -1 if there is no more data because
	 * 		the end of the stream has been reached.
	 */
	public int read(byte[] buffer, int offset, int length) {
		for(int k = 0; k < length; k++)
			if(this.offset < this.length)
				buffer[offset + k] = data[this.offset++];
			else
				buffer[offset + k] = -1;
		return length;
	}

	/**
	 * Reads the next byte of data from the input stream.
	 * The value byte is returned as an <code>int</code> in the range 0 to 255.
	 * If no byte is available because the end of the stream has been reached,
	 * the value -1 is returned. This method blocks until input
	 * data is available, the end of the stream is detected,
	 * or an exception is thrown.
	 * 
	 * @return the next byte of data, or -1 if the end of the stream is reached.
	 */
	public int read() {
		byte temp[] = new byte[1];
		read(temp, 0, 1);
		return temp[0];
	}
}