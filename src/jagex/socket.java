package jagex;

import java.io.IOException;
import java.math.BigInteger;

public class socket {

	private static final int cmdcount[] = new int[256];

	public static final int lencount[] = new int[256];

	protected final int zd = 61, ae = 59, be = 42, ce = 43,
	de = 44, ee = 45, fe = 46, ge = 47, he = 92,
	ie = 32, je = 124, ke = 34;

	protected int dlen, packcnt;
	
	protected boolean error;

	protected String errortext;

	private byte data[];

	protected int packetlen;
	
	public int read, maxcnt;
	
	private int start, pos, skip;

	public socket() {
		dlen = 5000;
		error = false;
		errortext = "";
		pos = 3;
		skip = 8;
	}

	public void close() { }

	public void read(int len, int off, byte[] buff) throws IOException { }

	public void writebuf(byte[] buf, int off, int len) throws IOException { }

	public int read() throws IOException {
		return 0;
	}

	public int available() throws IOException {
		return 0;
	}

	public int zb(byte[] abyte0, int i, int j) throws IOException {
		return 0;
	}

	public int g1() throws IOException {
		return read();
	}

	public int g2() throws IOException {
		int i = g1();
		int j = g1();
		return i * 256 + j;
	}

	public int g4() throws IOException {
		int i = g2();
		int j = g2();
		return i * 0x10000 + j;
	}

	public void read(int len, byte[] buff) throws IOException {
		read(len, 0, buff);
	}

	public int readbuf(byte[] data) {
		try {
			read++;
			if (maxcnt > 0 && read > maxcnt) {
				error = true;
				errortext = "time-out";
				maxcnt += maxcnt;
				return 0;
			}
			if (packetlen == 0 && available() >= 2) {
				packetlen = read();
				if (packetlen >= 160) {
					packetlen = (packetlen - 160) * 256 + read();
				}
			}
			if (packetlen > 0 && available() >= packetlen) {
				if (packetlen >= 160) {
					read(packetlen, data);
				} else {
					data[packetlen - 1] = (byte)read();
					if (packetlen > 1) {
						read(packetlen - 1, data);
					}
				}
				int length = packetlen;
				packetlen = 0;
				read = 0;
				return length;
			}
		} catch(IOException e) {
			error = true;
			errortext = e.getMessage();
		}
		return 0;
	}

	public void p1(int val) {
		data[pos++] = (byte) val;
	}

	public void p2(int val) {
		data[pos++] = (byte)(val >> 8);
		data[pos++] = (byte) val;
	}

	public void psize2(int val, int off) {
		data[start + off] = (byte)(val >> 8);
		data[start + off + 1] = (byte) val;
	}

	public void p4(int val) {
		data[pos++] = (byte)(val >> 24);
		data[pos++] = (byte)(val >> 16);
		data[pos++] = (byte)(val >> 8);
		data[pos++] = (byte) val;
	}

	public void p8(long val) {
		p4((int)(val >> 32));
		p4((int)(val & -1L));
	}

	@SuppressWarnings("deprecation")
	public void pjstr(String s) {
		s.getBytes(0, s.length(), data, pos);
		pos += s.length();
	}

	public void pdata(byte[] buf, int off, int len) {
		for (int i = 0; i < len; i++) {
			data[pos++] = buf[off + i];
		}
	}

	public void prsa8(long val, int sessionid, BigInteger exponent, BigInteger modulus) {
		byte dat[] = new byte[15];
		dat[0] = (byte)(int)(1.0D + Math.random() * 127D);
		dat[1] = (byte)(int)(Math.random() * 256D);
		dat[2] = (byte)(int)(Math.random() * 256D);
		util.p4(dat, 3, sessionid);
		util.p8(dat, 7, val);
		BigInteger biginteger2 = new BigInteger(1, dat);
		BigInteger biginteger3 = biginteger2.modPow(exponent, modulus);
		byte abyte1[] = biginteger3.toByteArray();
		data[pos++] = (byte) abyte1.length;
		for (int j = 0; j < abyte1.length; j++)
			data[pos++] = abyte1[j];
	}

	public void prsastr(String str, int sessionid, BigInteger exponent, BigInteger modulus) {
		byte bytes[] = str.getBytes();
		int strlen = bytes.length;
		byte magnitude[] = new byte[15];
		
		for (int i = 0; i < strlen; i += 7) {
			magnitude[0] = (byte)(int)(1.0D + Math.random() * 127D);
			magnitude[1] = (byte)(int)(Math.random() * 256D);
			magnitude[2] = (byte)(int)(Math.random() * 256D);
			magnitude[3] = (byte)(int)(Math.random() * 256D);
			util.p4(magnitude, 4, sessionid);

			for (int l = 0; l < 7; l++) {
				if (i + l < strlen) {
					magnitude[8 + l] = bytes[i + l];
				} else {
					magnitude[8 + l] = 32;
				}
			}

			BigInteger bigint = new BigInteger(1, magnitude).modPow(exponent, modulus);
			byte dat[] = bigint.toByteArray();
			data[pos++] = (byte) dat.length;
			for (int j = 0; j < dat.length; j++) {
				data[pos++] = dat[j];
			}
		}

	}

	public void create(int id) {
		if (start > (dlen * 4) / 5) {
			try {
				wdata(0);
			} catch(IOException e) {
				error = true;
				errortext = e.getMessage();
			}
		}
		if (data == null)
			data = new byte[dlen];
		data[start + 2] = (byte) id;
		data[start + 3] = 0;
		pos = start + 3;
		skip = 8;
	}

	public void fmtdata() {
		if (skip != 8)
			pos++;
		int len = pos - start - 2;
		
		if (len >= 160) {
			data[start] = (byte) (160 + len / 256);
			data[start + 1] = (byte) (len & 0xff);
		} else {
			data[start] = (byte) len;
			pos--;
			data[start + 1] = data[pos];
		}
		if (dlen <= 10000) {
			int j = data[start + 2] & 0xff;
			cmdcount[j]++;
			lencount[j] += pos - start;
		}
		start = pos;
	}

	public void flush() throws IOException {
		fmtdata();
		wdata(0);
	}

	public void wdata(int i) throws IOException {
		if(error) {
			start = 0;
			pos = 3;
			error = false;
			throw new IOException(errortext);
		}
		packcnt++;
		if (packcnt < i)
			return;
		if (start > 0) {
			packcnt = 0;
			writebuf(data, 0, start);
		}
		start = 0;
		pos = 3;
	}

	public boolean any() {
		return start > 0;
	}

}