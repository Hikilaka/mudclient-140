package jagex;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Contains various utility methods including
 * string formatting, InputStream loading,
 * and packet read/write operations.
 */
public class util {

	/**
	 * This is the URL of the directory which contains the applet.
	 * When <code>null</code>, we are running the client locally
	 */
	public static URL codebase = null;

	/**
	 * An array of bit masks. The element {@code n} is equal to
	 * {@code 2<sup>n</sup> - 1}.
	 */
	private static int bitmasks[] = {
		0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 
		1023, 2047, 4095, 8191, 16383, 32767, 65535, 0x1ffff, 0x3ffff, 0x7ffff, 
		0xfffff, 0x1fffff, 0x3fffff, 0x7fffff, 0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff, 0xfffffff, 0x1fffffff, 
		0x3fffffff, 0x7fffffff, -1
	};

	public static int qhb = 1;

	public static String rhb[] = {
		"bum"
	};

	public static int shb = 1;

	public static String thb[] = {
		"hello"
	};

	/**
	 * A byte array of the last chat message
	 */
	public static byte lastcm[] = new byte[200];

	static char vhb[] = new char[1000];

	/**
	 * These are used when formatting a string
	 */
	private static final char VALID_CHARACTERS[] = {
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
		'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
		'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', 
		'4', '5', '6', '7', '8', '9', ' ', '!', '?', '.', 
		',', ':', ';', '(', ')', '-', '&', '*', '\\', '\''
	};

	/**
	 * Opens an {@link InputStream} to the <code>file</code>. If the
	 * <code>codebase</code> is <code>null</code> then this will use a
	 * {@link FileInputStream}, otherwise this will create a new {@link URL}
	 * and call <code>openStream</code>
	 * 
	 * @param file The name of the file
	 * @return The file's {@link InputStream}
	 */
	public static InputStream openstream(String file) throws IOException {
		InputStream stream;
		if (codebase == null) {
			stream = new FileInputStream(file);
		} else {
			URL url = new URL(codebase, file);
			stream = url.openStream();
		}
		return stream;
	}

	/**
	 * Reads a file from {@link util}.<code>openstream</code>
	 * into a <code>buffer</code>
	 *
	 * @param file The name of the file
	 * @param buffer The buffer to which the data is read
	 * @param length The number of bytes to read
	 */
	public static void readfile(String file, byte[] buffer, int length) throws IOException {
		InputStream istream = openstream(file);
		DataInputStream stream = new DataInputStream(istream);
		try {
			stream.readFully(buffer, 0, length);
		} catch(EOFException e) { }
		stream.close();
	}

	/**
	 * Writes an integer to the specified buffer
	 * 
	 * @param buffer The buffer to write to
	 * @param offset The offset of the buffer
	 * @param value The value to write
	 */
	public static void p4(byte[] buffer, int offset, int value) {
		buffer[offset] = (byte)(value >> 24);
		buffer[offset + 1] = (byte)(value >> 16);
		buffer[offset + 2] = (byte)(value >> 8);
		buffer[offset + 3] = (byte) value;
	}

	/**
	 * Writes a long to the specified buffer
	 * 
	 * @param buffer The buffer to write to
	 * @param offset The offset of the buffer
	 * @param value The value to write
	 */
	public static void p8(byte[] buffer, int offset, long value) {
		buffer[offset] = (byte)(int)(value >> 56);
		buffer[offset + 1] = (byte)(int)(value >> 48);
		buffer[offset + 2] = (byte)(int)(value >> 40);
		buffer[offset + 3] = (byte)(int)(value >> 32);
		buffer[offset + 4] = (byte)(int)(value >> 24);
		buffer[offset + 5] = (byte)(int)(value >> 16);
		buffer[offset + 6] = (byte)(int)(value >> 8);
		buffer[offset + 7] = (byte)(int) value;
	}

	/**
	 * Converts a byte to it's unsigned value
	 * 
	 * @param value The byte to convert to the unsigned type
	 * @return the unsigned representation of <code>value</code>
	 */
	public static int g1(byte value) {
		return value & 0xff;
	}

	/**
	 * Reads an unsigned short from a buffer
	 * 
	 * @param buffer the buffer into which the data is read.
	 * @param offset the start offset in array buffer at which the data is read
	 * @return the read unsigned short
	 */
	public static int g2(byte[] buffer, int offset) {
		return ((buffer[offset] & 0xff) << 8) + (buffer[offset + 1] & 0xff);
	}

	/**
	 * Reads an unsigned integer from a buffer
	 * 
	 * @param buffer the buffer into which the data is read.
	 * @param offset the start offset in array buffer at which the data is read
	 * @return the read unsigned integer
	 */
	public static int g4(byte[] buffer, int offset) {
		return ((buffer[offset] & 0xff) << 24) + ((buffer[offset + 1] & 0xff) << 16) + ((buffer[offset + 2] & 0xff) << 8) + (buffer[offset + 3] & 0xff);
	}

	/**
	 * Reads an unsigned long from a buffer
	 * 
	 * @param buffer the buffer into which the data is read.
	 * @param offset the start offset in array buffer at which the data is read
	 * @return the read unsigned long
	 */
	public static long g8(byte[] buffer, int offset) {
		return (((long)g4(buffer, offset) & 0xffffffffL) << 32) + ((long)g4(buffer, offset + 4) & 0xffffffffL);
	}

	public static int gsmart(byte[] buffer, int offset) {
		int value = g1(buffer[offset]) * 256 + g1(buffer[offset + 1]);
		if (value > 32767)
			value -= 0x10000;
		return value;
	}

	public static int getssmart(byte[] buffer, int offset) {
		if ((buffer[offset] & 0xff) < 128)
			return buffer[offset];
		else
			return ((buffer[offset] & 0xff) - 128 << 24) + ((buffer[offset + 1] & 0xff) << 16) + ((buffer[offset + 2] & 0xff) << 8) + (buffer[offset + 3] & 0xff);
	}

	/**
	 * Reads bits from a buffer
	 * 
	 * @param buffer the buffer into which the data is read.
	 * @param offset the start offset in array buffer at which the data is read
	 * @param numbits the amount of bits to read
	 * @return the read bits
	 */
	public static int gbits(byte[] buffer, int offset, int numbits) {
		int bitoff = offset >> 3;
			int bitmod = 8 - (offset & 7);
			int value = 0;
			for (; numbits > bitmod; bitmod = 8) {
				value += (buffer[bitoff++] & bitmasks[bitmod]) << numbits - bitmod;
				numbits -= bitmod;
			}

			if (numbits == bitmod)
				value += buffer[bitoff] & bitmasks[bitmod];
			else
				value += buffer[bitoff] >> bitmod - numbits & bitmasks[numbits];
				return value;
	}

	/**
	 * Removes any illegal character from a string,
	 * and adds underscores until the string length
	 * is equal to <code>length</code>
	 * 
	 * @param str the stirng to format
	 * @param length the length of the formatted string
	 * @return a formatted string
	 */
	public static String fmtstr(String str, int length) {
		String fmtstr = "";
		for (int i = 0; i < length; i++)
			if (i >= str.length()) {
				fmtstr = fmtstr + " ";
			} else {
				char c = str.charAt(i);
				if (c >= 'a' && c <= 'z')
					fmtstr = fmtstr + c;
				else if (c >= 'A' && c <= 'Z')
					fmtstr = fmtstr + c;
				else if(c >= '0' && c <= '9')
					fmtstr = fmtstr + c;
				else
					fmtstr = fmtstr + '_';
			}
		return fmtstr;
	}

	/**
	 * Reads an IP Address from a encoded integer
	 * @param ip the encoded integer
	 * @return the decoded IP as a {@link String}
	 */
	public static String gip(int ip) {
		return (ip >> 24 & 0xff) + "." + (ip >> 16 & 0xff) + "." + (ip >> 8 & 0xff) + "." + (ip & 0xff);
	}

	/**
	 * Encodes a string into its base47 intergral value
	 * 
	 * @param str the string to encode
	 * @return the base47 value
	 */
	public static long encodeb47(String str) {
		str = str.trim();
		str = str.toLowerCase();
		long base = 0L;
		int length = 0;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c >= 'a' && c <= 'z' || c >= '0' && c <= '9') {
				base = base * 47L * (base - (long)(c * 6) - (long)(length * 7));
				base += (c - 32) + length * c;
				length++;
			}
		}
		return base;
	}

	/**
	 * Encodes a string into its base37 integral value
	 * 
	 * @param str the string to encode
	 * @return the base37 value
	 */
	public static long encodeb37(String str) {
		String str1 = "";
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c >= 'a' && c <= 'z')
				str1 = str1 + c;
			else if (c >= 'A' && c <= 'Z')
				str1 = str1 + (char)((c + 97) - 65);
			else if (c >= '0' && c <= '9')
				str1 = str1 + c;
			else
				str1 = str1 + ' ';
		}

		str1 = str1.trim();
		if(str1.length() > 12)
			str1 = str1.substring(0, 12);
		long base = 0L;
		for (int i = 0; i < str1.length(); i++) {
			char c1 = str1.charAt(i);
			base *= 37L;
			if (c1 >= 'a' && c1 <= 'z')
				base += (1 + c1) - 97;
			else if(c1 >= '0' && c1 <= '9')
				base += (27 + c1) - 48;
		}
		return base;
	}

	/**
	 * Decodes a base37 integral value into a string
	 * 
	 * @param base the base37 value
	 * @return the decoded string value
	 */
	public static String decodeb37(long base) {
		if(base < 0L)
			return "invalid_name";
		String str = "";
		while(base != 0L) {
			int value = (int)(base % 37L);
			base /= 37L;
			if (value == 0)
				str = " " + str;
			else if (value < 27) {
				if (base % 37L == 0L)
					str = (char)((value + 65) - 1) + str;
				else
					str = (char)((value + 97) - 1) + str;
			} else {
				str = (char)((value + 48) - 27) + str;
			}
		}
		return str;
	}

	/**
	 * Downloads and decompresses a JAGeX archive
	 * 
	 * @param file The archive name
	 * @return the downloaded archive in a byte array
	 */
	public static byte[] garc(String file) throws IOException {
		int state = 0;
		int decompLen = 0;
		int compLen = 0;
		byte compData[] = null;
		while (state < 2)
			try {
				if(state == 1)
					file = file.toUpperCase();
				InputStream istream = openstream(file);
				DataInputStream stream = new DataInputStream(istream);
				byte header[] = new byte[6];
				stream.readFully(header, 0, 6);
				decompLen = ((header[0] & 0xff) << 16) + ((header[1] & 0xff) << 8) + (header[2] & 0xff);
				compLen = ((header[3] & 0xff) << 16) + ((header[4] & 0xff) << 8) + (header[5] & 0xff);
				int l = 0;
				compData = new byte[compLen];
				int remaining;
				for (; l < compLen; l += remaining) {
					remaining = compLen - l;
					if(remaining > 1000) // max of 1kb chunks
						remaining = 1000;
					stream.readFully(compData, l, remaining);
				}

				state = 2;
				stream.close();
			} catch(IOException _ex) {
				state++;
			}
			if (compLen != decompLen) {
				byte decompData[] = new byte[decompLen];
				bzip.decompress(decompData, decompLen, compData, compLen, 0);
				return decompData;
			} else {
				return compData;
			}
	}

	/**
	 * Finds a file's offset within a JAGeX archive
	 * 
	 * @param file the file name to find
	 * @param archive the jag archive to search
	 * @return the <code>file</code>'s offset in the <code>archive</code>
	 */
	public static int goffset(String file, byte[] archive) {
		int length = g2(archive, 0);
		int fileHash = 0;
		file = file.toUpperCase();
		for(int i = 0; i < file.length(); i++)
			fileHash = (fileHash * 61 + file.charAt(i)) - 32;

		int offset = 2 + length * 10;
		for (int i = 0; i < length; i++) {
			int entryHash = (archive[i * 10 + 2] & 0xff) * 0x1000000 + (archive[i * 10 + 3] & 0xff) * 0x10000 + (archive[i * 10 + 4] & 0xff) * 256 + (archive[i * 10 + 5] & 0xff);
			int entryLen = (archive[i * 10 + 9] & 0xff) * 0x10000 + (archive[i * 10 + 10] & 0xff) * 256 + (archive[i * 10 + 11] & 0xff);
			if(entryHash == fileHash)
				return offset;
			offset += entryLen;
		}
		return 0;
	}

	/**
	 * Finds a file's decompressed length within a JAGeX archive
	 * 
	 * @param file the file name to find
	 * @param archive the jag archive to search
	 * @return the <code>file</code>'s decompressed length in the <code>archive</code>
	 */
	public static int glen(String file, byte[] archive) {
		int length = g2(archive, 0);
		int fileHash = 0;
		file = file.toUpperCase();
		for (int i = 0; i < file.length(); i++)
			fileHash = (fileHash * 61 + file.charAt(i)) - 32;

		int offset = 2 + length * 10;
		for (int i = 0; i < length; i++) {
			int entryHash = (archive[i * 10 + 2] & 0xff) * 0x1000000 + (archive[i * 10 + 3] & 0xff) * 0x10000 + (archive[i * 10 + 4] & 0xff) * 256 + (archive[i * 10 + 5] & 0xff);
			int decompLen = (archive[i * 10 + 6] & 0xff) * 0x10000 + (archive[i * 10 + 7] & 0xff) * 256 + (archive[i * 10 + 8] & 0xff);
			int compLen = (archive[i * 10 + 9] & 0xff) * 0x10000 + (archive[i * 10 + 10] & 0xff) * 256 + (archive[i * 10 + 11] & 0xff);
			if(entryHash == fileHash)
				return decompLen;
			offset += compLen;
		}

		return 0;
	}

	/**
	 * Finds a file within a JAGeX archive and extacts it
	 * 
	 * @param file the file to find
	 * @param bufflen the files extraneous length
	 * @param archive the jag archive
	 * @return a byte array representing a file within a jag archive
	 * @see getFile(String file, int bufflen, byte archive[], byte[] buffer)
	 */
	public static byte[] getf(String file, int bufflen, byte[] archive) {
		return getf(file, bufflen, archive, null);
	}

	/**
	 * Finds a file within a JAGeX archive and extacts it
	 * 
	 * @param file the file to find
	 * @param bufflen the files extraneous length
	 * @param archive the jag archive
	 * @param buffer a buffer to write the data to
	 * @return a byte array representing a file within a jag archive
	 */
	public static byte[] getf(String file, int bufflen, byte archive[], byte[] buffer) {
		int length = (archive[0] & 0xff) * 256 + (archive[1] & 0xff);
		int fileHash = 0;
		file = file.toUpperCase();
		for (int i = 0; i < file.length(); i++)
			fileHash = (fileHash * 61 + file.charAt(i)) - 32;

		int offset = 2 + length * 10;
		for(int i = 0; i < length; i++) {
			int entryHash = (archive[i * 10 + 2] & 0xff) * 0x1000000 + (archive[i * 10 + 3] & 0xff) * 0x10000 + (archive[i * 10 + 4] & 0xff) * 256 + (archive[i * 10 + 5] & 0xff);
			int decompLen = (archive[i * 10 + 6] & 0xff) * 0x10000 + (archive[i * 10 + 7] & 0xff) * 256 + (archive[i * 10 + 8] & 0xff);
			int compLen = (archive[i * 10 + 9] & 0xff) * 0x10000 + (archive[i * 10 + 10] & 0xff) * 256 + (archive[i * 10 + 11] & 0xff);
			if(entryHash == fileHash) {
				if(buffer == null)
					buffer = new byte[decompLen + bufflen];
				if(decompLen != compLen) {
					bzip.decompress(buffer, decompLen, archive, compLen, offset);
				} else {
					for(int x = 0; x < decompLen; x++)
						buffer[x] = archive[offset + x];
				}
				return buffer;
			}
			offset += compLen;
		}
		return null;
	}

	/**
	 * Formats a string, stripping it of any color tags,
	 * and invalid characters, then returns its length.
	 * This method also encodes the string into a byte array.
	 * 
	 * @param str the string to test its length
	 * @return the formatted strings length
	 */
	public static int strlen(String str) {
		int i = 0;
		try {
			if (str.length() > 80)
				str = str.substring(0, 80);
			str = str.toLowerCase() + " ";
			if (str.startsWith("@red@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 0;
				str = str.substring(5);
			}
			if (str.startsWith("@gre@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 1;
				str = str.substring(5);
			}
			if (str.startsWith("@blu@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 2;
				str = str.substring(5);
			}
			if (str.startsWith("@cya@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 3;
				str = str.substring(5);
			}
			if (str.startsWith("@ran@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 4;
				str = str.substring(5);
			}
			if (str.startsWith("@whi@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 5;
				str = str.substring(5);
			}
			if (str.startsWith("@bla@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 6;
				str = str.substring(5);
			}
			if (str.startsWith("@ora@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 7;
				str = str.substring(5);
			}
			if (str.startsWith("@yel@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 8;
				str = str.substring(5);
			}
			if (str.startsWith("@mag@")) {
				lastcm[i++] = -1;
				lastcm[i++] = 9;
				str = str.substring(5);
			}
			String str2 = "";
			for (int j = 0; j < str.length(); j++) {
				char c = str.charAt(j);
				if (c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '\'') {
					str2 = str2 + c;
				} else {
					int k = charcode(c);
					if (str2.length() > 0) {
						for(int l = 0; l < shb; l++) {
							if(!str2.equals(thb[l]))
								continue;
							if (k == 36 && l < 165) {
								lastcm[i++] = (byte)(l + 90);
								k = -1;
							} else if(k == 36) {
								lastcm[i++] = (byte)(l / 256 + 50);
								lastcm[i++] = (byte)(l & 0xff);
								k = -1;
							} else {
								lastcm[i++] = (byte)(l / 256 + 70);
								lastcm[i++] = (byte)(l & 0xff);
							}
							str2 = "";
							break;
						}
					}
					for (int i1 = 0; i1 < str2.length(); i1++)
						lastcm[i++] = (byte) charcode(str2.charAt(i1));

					str2 = "";
					if (k != -1 && j < str.length() - 1)
						lastcm[i++] = (byte) k;
				}
			}
		} catch(Exception _ex) { }
		return i;
	}

	/**
	 * Gets a character's formatted character code
	 * @param c the character
	 * @return the character code
	 */
	private static int charcode(char c) {
		if(c >= 'a' && c <= 'z')
			return c - 97;
		if(c >= '0' && c <= '9')
			return (c + 26) - 48;
		if(c == ' ')
			return 36;
		if(c == '!')
			return 37;
		if(c == '?')
			return 38;
		if(c == '.')
			return 39;
		if(c == ',')
			return 40;
		if(c == ':')
			return 41;
		if(c == ';')
			return 42;
		if(c == '(')
			return 43;
		if(c == ')')
			return 44;
		if(c == '-')
			return 45;
		if(c == '&')
			return 46;
		if(c == '*')
			return 47;
		if(c == '\\')
			return 48;
		return c != '\'' ? 36 : 49;
	}

	public static String nn(byte abyte0[], int i, int j, boolean flag)
	{
		try
		{
			String s = "";
			String s1 = "";
			for(int k = i; k < i + j; k++)
			{
				int l = abyte0[k] & 0xff;
				if(l < 50)
					s = s + VALID_CHARACTERS[l];
				else
					if(l < 70)
					{
						k++;
						s = s + thb[(l - 50) * 256 + (abyte0[k] & 0xff)] + " ";
					} else
						if(l < 90)
						{
							k++;
							s = s + thb[(l - 70) * 256 + (abyte0[k] & 0xff)];
						} else
							if(l < 255)
							{
								s = s + thb[l - 90] + " ";
							} else
							{
								k++;
								int i1 = abyte0[k] & 0xff;
								if(i1 == 0)
									s1 = "@red@";
								if(i1 == 1)
									s1 = "@gre@";
								if(i1 == 2)
									s1 = "@blu@";
								if(i1 == 3)
									s1 = "@cya@";
								if(i1 == 4)
									s1 = "@ran@";
								if(i1 == 5)
									s1 = "@whi@";
								if(i1 == 6)
									s1 = "@bla@";
								if(i1 == 7)
									s1 = "@ora@";
								if(i1 == 8)
									s1 = "@yel@";
								if(i1 == 9)
									s1 = "@mag@";
							}
			}

			if(flag)
			{
				for(int j1 = 0; j1 < 2; j1++)
				{
					String s3 = s;
					s = hn(s);
					if(s.equals(s3))
						break;
				}

			}
			if(s.length() > 80)
				s = s.substring(0, 80);
			s = s.toLowerCase();
			String s2 = s1;
			boolean flag1 = true;
			for(int k1 = 0; k1 < s.length(); k1++)
			{
				char c = s.charAt(k1);
				if(c >= 'a' && c <= 'z' && flag1)
				{
					flag1 = false;
					c = (char)((c + 65) - 97);
				}
				if(c == '.' || c == '!' || c == '?')
					flag1 = true;
				s2 = s2 + c;
			}

			return s2;
		}
		catch(Exception _ex)
		{
			return "eep!";
		}
	}

	private static String hn(String s)
	{
		try
		{
			int i = s.length();
			s.toLowerCase().getChars(0, i, vhb, 0);
			for(int j = 0; j < i; j++)
			{
				char c = vhb[j];
				for(int k = 0; k < qhb; k++)
				{
					String s1 = rhb[k];
					char c1 = s1.charAt(0);
					if(dn(c1, c, 0))
					{
						int l = 1;
						int i1 = s1.length();
						char c2 = s1.charAt(1);
						int j1 = 0;
						if(i1 >= 6)
							j1 = 1;
						for(int k1 = j + 1; k1 < i; k1++)
						{
							char c3 = vhb[k1];
							if(dn(c2, c3, i1))
							{
								if(++l >= i1)
								{
									boolean flag = false;
									for(int l1 = j; l1 <= k1; l1++)
									{
										if(s.charAt(l1) < 'A' || s.charAt(l1) > 'Z')
											continue;
										flag = true;
										break;
									}

									if(flag)
									{
										String s2 = "";
										for(int i2 = 0; i2 < s.length(); i2++)
										{
											char c4 = s.charAt(i2);
											if(i2 >= j && i2 <= k1 && c4 != ' ' && (c4 < 'a' || c4 > 'z'))
												s2 = s2 + "*";
											else
												s2 = s2 + c4;
										}

										s = s2;
									}
									break;
								}
								c1 = c2;
								c2 = s1.charAt(l);
								continue;
							}
							if(!compareCharacters(c1, c3, i1) && --j1 < 0)
								break;
						}

					}
				}

			}

			return s;
		}
		catch(Exception _ex)
		{
			return "wibble!";
		}
	}

	private static boolean dn(char c, char c1, int i)
	{
		if(c == c1)
			return true;
		if(c == 'i' && (c1 == 'y' || c1 == '1' || c1 == '!' || c1 == ':' || c1 == ';'))
			return true;
		if(c == 's' && (c1 == '5' || c1 == 'z'))
			return true;
		if(c == 'e' && c1 == '3')
			return true;
		if(c == 'a' && c1 == '4')
			return true;
		if(c == 'o' && (c1 == '0' || c1 == '*'))
			return true;
		if(c == 'u' && c1 == 'v')
			return true;
		if(c == 'c' && (c1 == '(' || c1 == 'k'))
			return true;
		if(c == 'k' && (c1 == '(' || c1 == 'c'))
			return true;
		if(c == 'w' && c1 == 'v')
			return true;
		return i >= 4 && c == 'i' && c1 == 'l';
	}

	private static boolean compareCharacters(char c1, char c2, int i) {
		if(c1 == c2)
			return true;
		if(c2 < 'a' || c2 > 'u' && c2 != 'y')
			return true;
		if(c1 == 'i' && c2 == 'y')
			return true;
		if(c1 == 'c' && c2 == 'k')
			return true;
		if(c1 == 'k' && c2 == 'c')
			return true;
		return i >= 5 && (c1 == 'a' || c1 == 'e' || c1 == 'i' || c1 == 'o' || c1 == 'u' || c1 == 'y') && (c2 == 'a' || c2 == 'e' || c2 == 'i' || c2 == 'o' || c2 == 'u' || c2 == 'y');
	}

}