import jagex.client.graphics;
import java.awt.Component;

public final class gamegraphics extends graphics {

	public mudclient client;

	public gamegraphics(int width, int heigth, int spritelen, Component component) {
		super(width, heigth, spritelen, component);
	}

	public void drawentity(int j, int k, int l, int i1, int id, int k1, int l1) {
		if (id >= 50000) {
			client.yl(j, k, l, i1, id - 50000, k1, l1);
		} else if (id >= 40000) {
			client.vm(j, k, l, i1, id - 40000, k1, l1);
		} else if (id >= 20000) {
			client.an(j, k, l, i1, id - 20000, k1, l1);
		} else if (id >= 5000) {
			client.ml(j, k, l, i1, id - 5000, k1, l1);
		} else {
			super.nf(j, k, l, i1, id);
		}
	}

}