public final class version {

	public final static int MAJOR_VERSION = 140;

	public final static int MINOR_VERSION = 140;

	public final static int CONFIG_VERSION = 61;

	public final static int LANDSCAPE_VERSION = 39;

	public final static int MEDIA_VERSION = 38;

	public final static int MODEL_VERSION = 18;

	public final static int TEXTURE_VERSION = 12;

	public final static int ENTITY_VERSION = 15;

	public final static int SOUNDS_VERSION = 1;

}